<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });


Route::get('invitation/{id_text}/{to}', [InvitationController::class, 'index']);

Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/authenticate', [AuthController::class, 'authenticate']);

Route::group(['middleware' => 'auth'], function () {

    Route::get('/logout', [AuthController::class, 'logout']);
    Route::get('/', [DashboardController::class, 'index'])->name('home');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('home');

    // Template
    Route::get('/product', [ProductController::class, 'index']);
    Route::get('/product/datatable', [ProductController::class, 'datatable']);
    Route::get('/product/create', [ProductController::class, 'create']);
    Route::post('/product/create_process', [ProductController::class, 'create_process']);
    Route::get('/product/update/{id}', [ProductController::class, 'update']);
    Route::post('/product/update_process/{id}', [ProductController::class, 'update_process']);
    Route::get('/product/detail/{id}', [ProductController::class, 'detail']);
});
