<?php

function hariIndo ($day) {
    switch ($day) {
      case 'Sun':
        return 'Minggu';
      case 'Mon':
        return 'Senin';
      case 'Tue':
        return 'Selasa';
      case 'Wed':
        return 'Rabu';
      case 'Thu':
        return 'Kamis';
      case 'Fri':
        return 'Jumat';
      case 'Sat':
        return 'Sabtu';
      default:
        return 'hari tidak valid';
    }
  }