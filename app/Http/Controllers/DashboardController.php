<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use Session;

class DashboardController extends Controller
{
    public function __construct(){
		if(!Session::get('login')) return redirect('login');
        return view('content.dashboard');
	}

    public function index($id = NULL) {
        // dd(Auth::user());
        return view('content.dashboard');
    }
}