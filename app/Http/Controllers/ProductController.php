<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Auth;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
    }

    public function index($id = NULL)
    {
        // dd(Auth::user());
        $data['page'] = 'Product';
        return view('content.product', compact('data'));
    }

    public function datatable(Request $request)
    {
        $search  = $_GET['search']['value']; //Search all column
        $columns = $request->input('columns'); //Search each column

        $search_column = array(
            "name" => (isset($columns[1]['search']['value'])) ? $columns[1]['search']['value'] : "",
            "sku" => (isset($columns[2]['search']['value'])) ? $columns[2]['search']['value'] : "",
            "status" => (isset($columns[3]['search']['value'])) ? $columns[3]['search']['value'] : "",
        );

        $limit = $request->input('length'); // Ambil data limit per page
        $start = $request->input('start'); // Ambil data start

        // Order By
        $order_index = $_GET['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        if ($order_index == 1)
            $order_field = 'name';
        else if ($order_index == 2)
            $order_field = 'sku';
        else if ($order_index == 3)
            $order_field = 'price';
        else if ($order_index == 4)
            $order_field = 'quantity';
        else if ($order_index == 5)
            $order_field = 'status';
        else
            $order_field = 'id';
        $order_ascdesc = $_GET['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"

        $sql_total = Product::all()->count();
        $sql_data  = Product::select('*')->orderByRaw($order_field . ' ' . $order_ascdesc);
        if ($search != '' && $search != NULL) {
            $sql_data->where(function ($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('sku', 'LIKE', "%{$search}%")
                    ->orWhere('price', 'LIKE', "%{$search}%")
                    ->orWhere('quantity', 'LIKE', "%{$search}%")
                    ->orWhere('status', 'LIKE', "%{$search}%");
            });
        }

        $sql_data->offset($start)->limit($limit); // Untuk menambahkan query LIMIT
        $sql_fetch = $sql_data->get();
        // dd($sql_fetch);

        foreach ($sql_fetch as $value) {
            $row = array();
            $action = '<a class="btn btn-success btn-xl" style="margin-right: 5px;" href="product/detail/' . base64_encode($value->id) . '"><i class="fa fa-search"></i> View</a>';
            $action .= '<a class="btn btn-info btn-xl" style="margin-right: 5px;" href="product/update/' . base64_encode($value->id) . '"><i class="fa fa-edit"></i> Edit</a>';


            $status = '<span class="badge bg-primary">Aktif</span>';

            $row[] = $action;
            $row[] = $value->name;
            $row[] = $value->sku;
            $row[] = number_format($value->price, 0, ',', '.');
            $row[] = $value->quantity;
            $row[] = $status;

            $data[] = $row;
        }

        $sql_filter = Product::select('*')->orderByRaw($order_field . ' ' . $order_ascdesc);
        if ($search != '' && $search != NULL) {
            $sql_data->where(function ($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('sku', 'LIKE', "%{$search}%")
                    ->orWhere('price', 'LIKE', "%{$search}%")
                    ->orWhere('quantity', 'LIKE', "%{$search}%")
                    ->orWhere('status', 'LIKE', "%{$search}%");
            });
        }

        $sql_filter = $sql_filter->count();

        if ($sql_filter == 0) {
            $data = 0;
        }

        $callback = array(
            'draw' => $_GET['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $data
        );
        header('Content-Type: application/json');
        return $callback;
    }

    public function create()
    {
        $data['action'] = 'create';
        $data['page'] = 'Product';

        // dd($data);
        return view('content.product_form', $data);
    }

    public function create_process(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:products',
            'sku' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ]);

        // dd(Auth::user()->id);

        $product = new Product();
        $product->user_id = Auth::user()->id;
        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->save();

        return redirect('product')->with('status', 'Data has been validated and insert');
    }

    public function update($id)
    {
        $data['action'] = 'update';
        $data['page'] = 'Product';
        $data['data'] = Product::find(base64_decode($id));
        $data['id'] = base64_decode($id);

        // dd($data);
        return view('content.product_form', $data);
    }

    public function update_process(Request $request, $id)
    {
        $cek_unique = Product::find(base64_decode($id))->first();
        if ($cek_unique->nama == $request->nama) {
            $validated = $request->validate([
                'name' => 'required',
                'sku' => 'required',
                'price' => 'required',
                'quantity' => 'required',            ]);
        } else {
            $validated = $request->validate([
                'name' => 'required|unique:products',
                'sku' => 'required',
                'price' => 'required',
                'quantity' => 'required',            ]);
        }
        // dd($request->nama);

        $product = Product::find(base64_decode($id));
        $product->user_id = Auth::user()->id;
        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->save();

        return redirect('product')->with('status', 'Data has been validated and update');
    }

    public function detail($id)
    {
        $data['action'] = 'detail';
        $data['page'] = 'Product';
        $data['data'] = Product::find(base64_decode($id));
        $data['id'] = base64_decode($id);

        // dd($data);
        return view('content.product_form', $data);
    }
}
