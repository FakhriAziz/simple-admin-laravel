@extends('template')

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-end">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
                            <li class="breadcrumb-item">{{ $page }}</li>
                            <li class="breadcrumb-item active">
                                @if ($action == 'create')
                                    Create
                                @elseif ($action == 'update')
                                    Update
                                @else
                                    Detail
                                @endif
                            </li>
                        </ol>
                    </div>
                    <h4 class="page-title">{{ $page }}</h4>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-12">
                <div class="card primary-form">
                    <div class="card-header">
                        <h4 class="card-title">General</h4>
                    </div>

                    <div class="card-body">
                        <form
                            action="@if ($action == 'update') /product/{{ $action }}_process/{{ base64_encode($id) }} @else /product/{{ $action }}_process @endif"
                            method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="name">Nama</label>
                                        <input type="text" class="@error('name') is-invalid @enderror form-control"
                                            id="name" name="name" placeholder="Masukkan nama produk"
                                            value="{{ $data->name ?? '' }}" @if ($action == 'detail')disabled @endif>
                                        @error('name')
                                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="sku">SKU</label>
                                        <input type="number" class="@error('sku') is-invalid @enderror form-control"
                                            id="sku" name="sku" placeholder="Masukkan sku"
                                            value="{{ $data->sku ?? '' }}" @if ($action == 'detail')disabled @endif>
                                        @error('sku')
                                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="price">Harga</label>
                                        <input type="number" class="@error('price') is-invalid @enderror form-control"
                                            id="price" name="price" placeholder="Masukkan harga"
                                            value="{{ $data->price ?? '' }}" @if ($action == 'detail')disabled @endif>
                                        @error('price')
                                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="quantity">Stok</label>
                                        <input type="number" class="@error('quantity') is-invalid @enderror form-control"
                                            id="quantity" name="quantity" placeholder="Masukkan stok awal"
                                            value="{{ $data->quantity ?? '' }}" @if ($action == 'detail')disabled @endif>
                                        @error('quantity')
                                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-end">
                                    <a href="{{ url('product') }}">
                                        <button type="button" class="btn btn-primary"
                                            style="margin-left: 3px;">Back</button>
                                    </a>
                                    <button type="submit" class="btn btn-de-primary" @if ($action == 'detail')disabled @endif>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('load_script')
    <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
    {{-- <script src="{{ asset('js/simple-datatables.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/datatable.init.js') }}"></script> --}}
    <script src="{{ asset('js/app.js') }}"></script>
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
@endsection

@section('script')
@endsection
